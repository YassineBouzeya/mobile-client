package be.kdg.mvmobile;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowManager;

import be.kdg.mvmobile.injection.AppComponent;
import be.kdg.mvmobile.injection.DaggerAppComponent;

public class MVApplication extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        FlowManager.init(this);

        Stetho.initializeWithDefaults(this);

        appComponent = DaggerAppComponent.builder().build();
    }
}
