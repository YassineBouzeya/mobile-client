package be.kdg.mvmobile.activities.register;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.error.ErrorHandler;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.dto.UserDto;
import be.kdg.mvmobile.util.UtilProgress;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";

    @Inject
    UserService userService;

    // UI References
    @BindView(R.id.registeractivity_progressbar) View progressBarView;
    @BindView(R.id.registeractivity_scrollview) View registerFormView;
    @BindView(R.id.registeractivity_edittext_username) TextInputEditText usernameView;
    @BindView(R.id.registeractivity_edittext_voornaam) TextInputEditText firstNameView;
    @BindView(R.id.registeractivity_edittext_achternaam) TextInputEditText lastNameView;
    @BindView(R.id.registeractivity_edittext_email) TextInputEditText emailView;
    @BindView(R.id.registeractivity_edittext_password) TextInputEditText passwordView;
    @BindView(R.id.registeractivity_edittext_repeat_password) TextInputEditText repeatPasswordView;
    @BindView(R.id.registeractivity_button_registreer) Button registerButtonView;
    @BindView(R.id.registeractivity_textview_date) TextView mDisplayDate;
    @BindView(R.id.registeractivity_textview_select_date) TextView selectDateView;

    private DatePickerDialog.OnDateSetListener dateSetListener;

    // FocusView declared globally to give focus on field with error
    private View focusView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Tell Butterknife to resolve the @BindView() annotations
        ButterKnife.bind(this);

        // Tell Dagger to inject the @Inject annotations
        DaggerAppComponent.builder().build().inject(this);

        addEventHandlers();
    }

    private void addEventHandlers() {
        selectDateView.setOnClickListener(view -> {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(
                    RegisterActivity.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    dateSetListener,
                    year,month,day);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        });

        dateSetListener = (datePicker, year, month, day) -> {
            month = month + 1;
            Log.d(TAG, "onDateSet: dd/mm/yyyy: " + day + "/" + month + "/" + year);

            String date = day + "/" + month + "/" + year;
            selectDateView.setText(date);
        };

        registerButtonView.setOnClickListener(v -> {
            attemptRegister();
        });
    }

    /**
     * In this method we will validate the form fields, if everything is correct then we will call
     * performRegister() where we will do the networking aspect of registering a user
     */
    private void attemptRegister() {
        /*
        We need to check whether the email address is valid
        We need to check whether the password matches the requirements
        We need to check whether the repeated password matches the first password
        We need to check whether the provided date of birth is valid
        */

        // Reset errors.
        usernameView.setError(null);
        firstNameView.setError(null);
        lastNameView.setError(null);
        emailView.setError(null);
        passwordView.setError(null);
        repeatPasswordView.setError(null);

        // Store values at the time of the register attempt.
        String username = usernameView.getText().toString();
        String firstName = firstNameView.getText().toString();
        String lastName = lastNameView.getText().toString();
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();
        String repeatPassword = repeatPasswordView.getText().toString();
        String dateOfBirth = selectDateView.getText().toString();

        boolean cancel = false;
        focusView = null;

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.registeractivity_error_username_required));
            focusView = usernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            usernameView.setError(getString(R.string.registeractivity_error_username_invalid));
            focusView = usernameView;
            cancel = true;
        }

        // Check for a valid pre-name
        if (!cancel) {
            if (TextUtils.isEmpty(firstName)) {
                firstNameView.setError(getString(R.string.registeractivity_error_prename));
                focusView = firstNameView;
                cancel = true;
            }
        }

        // Check for a valid family name
        if (!cancel) {
            if (TextUtils.isEmpty(lastName)) {
                lastNameView.setError(getString(R.string.registeractivity_error_surname));
                focusView = lastNameView;
                cancel = true;
            }
        }

        // Check for a valid email, if the user entered one
        if (!cancel) {
            if (TextUtils.isEmpty(email)) {
                emailView.setError(getString(R.string.registeractivity_error_email_required));
                focusView = emailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                emailView.setError(getString(R.string.registeractivity_error_email_invalid));
                focusView = emailView;
                cancel = true;
            }
        }

        // Check for a valid password, if the user entered one.
        if (!cancel) {
            if (TextUtils.isEmpty(password)) {
                passwordView.setError(getString(R.string.registeractivity_error_password_required));
                focusView = passwordView;
                cancel = true;
            } else if (!isPasswordValid(password)) {
                passwordView.setError(getString(R.string.registeractivity_error_password_invalid));
            }
        }

        // Check whether repeat password field is empty, if it's not empty check whether password is
        // the same as the other
        if (!cancel) {
            if (TextUtils.isEmpty(repeatPassword)) {
                repeatPasswordView.setError(getString(R.string.registeractivity_error_repeatpassword_required));
                focusView = repeatPasswordView;
                cancel = true;
            } else if (!repeatPassword.equals(password)) {
                repeatPasswordView.setError(getString(R.string.registeractivity_error_repeatpassword_invalid));
                focusView = repeatPasswordView;
                cancel = true;
            }
        }

        // Check whether date of birth is empty
        if (!cancel) {
            if (dateOfBirth.equals("Selecteer")) {
                mDisplayDate.setError(getString(R.string.registeractivity_error_dateofbirth));
                Toast.makeText(this, "Selecteer een datum!", Toast.LENGTH_LONG).show();
                focusView = mDisplayDate;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            UtilProgress.showProgress(true, this, registerFormView, progressBarView);
            performRegister(firstName, lastName, email, username, password, dateOfBirth);
        }
    }

    private boolean isUsernameValid(String username) {
        return username.length() > 2;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    private boolean isDateOfBirthValid(String dateOfBirth) {
        return false;
    }

    private void performRegister(String firstName, String lastName, String email, String username, String password, String birthday) {
        /*
        We need to check whether the username that the user wants to register with isn't taken yet
        We need to check whether the email address hasn't been used yet (?)
        We need to POST the user's credentials to persist them when the above is checked and okay
         */
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        // Check whether the user is connected to the INTERNET, if not then show a Toast
        if (networkInfo != null && networkInfo.isConnected()) {
            birthday = birthday.replace("/", "-");
            String[] splitBirthday = birthday.split("-");
            birthday = splitBirthday[2] + "-" + splitBirthday[1] + "-" + splitBirthday[0];
            UserDto user = new UserDto(firstName, lastName, email, username, password, birthday);
            Call<Boolean> usernameResponse = userService.checkUsername(username);
            usernameResponse.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (!response.body()) {
                            // Response is a boolean so we check here whether the body of response is true
                            // or false, if it's true we know the username is already taken and need to stop
                            // the register attempt

                            Call<Boolean> emailResponse = userService.checkEmail(email);
                            emailResponse.enqueue(new Callback<Boolean>() {
                                @Override
                                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                    if (response.isSuccessful() && response.body() != null) {
                                        if (!response.body()) {
                                            // Response is a boolean so if it's true then we know
                                            // email is taken and need to stop the register attempt

                                            Call<Void> registerResponse = userService.register(user);
                                            registerResponse.enqueue(new Callback<Void>() {
                                                @Override
                                                public void onResponse(Call<Void> call, Response<Void> response) {
                                                    // We don't check whether the body is null here because we don't receive a response
                                                    // from the backend
                                                    if (response.isSuccessful()) {
                                                        UtilProgress.showProgress(false, getApplicationContext(), registerFormView, progressBarView);
                                                        Toast.makeText(getApplicationContext(), getString(R.string.registeractivity_registered), Toast.LENGTH_LONG).show();

                                                        // Shut down Activity to go back to StartActivity
                                                        finish();
                                                    } else {
                                                        ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), getString(R.string.retrofit_error_not_successful), TAG, getApplicationContext());
                                                        showProgress(false);
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Void> call, Throwable t) {
                                                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "register onFailure: " + t.getMessage(), TAG, getApplicationContext());
                                                    showProgress(false);
                                                }
                                            });
                                        } else {
                                            // Email is already taken
                                            if (focusView != null) {
                                                // If focusView is not null then we clear the focus on that view
                                                // so we can set it to the new view
                                                focusView.clearFocus();
                                            }
                                            emailView.setError(getString(R.string.registeractivity_error_email_taken));
                                            focusView = emailView;
                                            focusView.requestFocus();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<Boolean> call, Throwable t) {
                                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong), "checkEmail onFailure: " + t.getMessage(), TAG, getApplicationContext());
                                    showProgress(false);
                                }
                            });
                        } else {
                            // Username is already taken
                            if (focusView != null) {
                                focusView.clearFocus();
                            }
                            usernameView.setError(getString(R.string.registeractivity_error_username_taken));
                            focusView = usernameView;
                            focusView.requestFocus();
                        }
                    } else {
                        ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong),
                                getString(R.string.retrofit_error_not_successful) + " and " + getString(R.string.retrofit_error_body_is_null), TAG, getApplicationContext());
                        showProgress(false);
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    ErrorHandler.handleError(getString(R.string.toast_error_something_went_wrong),
                            "checkUsername onFailure: " + t.getMessage(), TAG, getApplicationContext());
                    showProgress(false);
                }
            });
        } else {
            ErrorHandler.handleError(getString(R.string.toast_error_not_connected), "", TAG, this);
            showProgress(false);
        }
    }

    private void showProgress(boolean show) {
        UtilProgress.showProgress(show, this, registerFormView, progressBarView);
    }
}
