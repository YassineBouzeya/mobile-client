package be.kdg.mvmobile.activities.start;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.activities.login.LoginActivity;
import be.kdg.mvmobile.activities.mainmenu.MainMenuActivity;
import be.kdg.mvmobile.activities.register.RegisterActivity;
import be.kdg.mvmobile.activities.rules.RulesActivity;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity {
    // UI References
    @BindView(R.id.startactivity_button_register) Button registerButton;
    @BindView(R.id.startactivity_button_sign_in) Button signInButton;
    @BindView(R.id.startactivity_button_spelregels) Button spelregelsButton;

    // MySharedPreferences
    private MySharedPreferences mySharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ButterKnife.bind(this);

        mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        // If user is logged in then he gets redirected to the main menu so he doesn't need to see
        // the start screen again
        if (isUserLoggedIn()) {
            Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        addEventHandlers();
    }

    private void addEventHandlers() {
        registerButton.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent);

        });

        signInButton.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        });

        spelregelsButton.setOnClickListener(v->{
            Intent intent = new Intent(getApplicationContext(), RulesActivity.class);
            startActivity(intent);
        });
    }

    /**
     * This method checks whether there is a token in the SharedPreferences
     * @return  true if token is present, this also means that the user is logged in
     *          false if token is not present, this means that the user shouldn't be sent through to the main menu
     */
    private boolean isUserLoggedIn() {
        // Check whether Token is stored in SharedPreferences
        return mySharedPreferences.readTokenSP() != null;
    }

}
