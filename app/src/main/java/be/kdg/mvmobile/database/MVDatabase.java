package be.kdg.mvmobile.database;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = MVDatabase.DATABASE_NAME, version = MVDatabase.VERSION)
public class MVDatabase {
    public static final String DATABASE_NAME = "MVDatabase";
    public static final int VERSION = 1;
}
