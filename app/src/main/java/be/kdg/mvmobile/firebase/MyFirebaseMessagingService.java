package be.kdg.mvmobile.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import javax.inject.Inject;

import androidx.core.app.NotificationCompat;
import be.kdg.mvmobile.R;
import be.kdg.mvmobile.activities.game.GameActivity;
import be.kdg.mvmobile.activities.mainmenu.MainMenuActivity;
import be.kdg.mvmobile.injection.DaggerAppComponent;
import be.kdg.mvmobile.model.users.User;
import be.kdg.mvmobile.services.UserService;
import be.kdg.mvmobile.services.dto.DeviceDto;
import be.kdg.mvmobile.sharedpreferences.MySharedPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FirebaseMessagingServ";

    private static final String CHANNEL_NAME = "MV";
    private static final String CHANNEL_DESC = "Machiavelli Messaging Channel";

    @Inject
    UserService userService;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String deviceToken) {
        Log.d(TAG, "Refreshed token: " + deviceToken);

        DaggerAppComponent.builder().build().inject(this);

        MySharedPreferences mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        User user = mySharedPreferences.readUserSP();

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(deviceToken, user, mySharedPreferences);
    }

    private void sendRegistrationToServer(String deviceToken, User user, MySharedPreferences mySharedPreferences) {
        if (user != null) {
            DeviceDto deviceDto = new DeviceDto();
            deviceDto.setUsername(user.getUsername());
            deviceDto.setDeviceId(deviceToken);
            Call<Void> sendDeviceIdResponse = userService.sendDeviceId(mySharedPreferences.readTokenSP().getAccess_token(), deviceDto);

            sendDeviceIdResponse.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "Het heeft gewerkt!", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Het heeft niet gewerkt!" + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

            user.setDeviceId(deviceToken);
            mySharedPreferences.writeUserSP(user);
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "Firebase message received");
        MySharedPreferences mySharedPreferences = new MySharedPreferences(this, getSharedPreferences(getString(R.string.be_kdg_mvmobile_myPreferences), MODE_PRIVATE));

        User user = mySharedPreferences.readUserSP();

        /**
         * In our message we don't have a notification part, we only have a data part so we will
         * always reach "onMessageReceived()". Check the Google Firebase documentation for notification
         * and data segments and how / where they get handled
         {
         "notification": {
         "id": 1,
         "title": "testGame",
         "body": "Jij bent aan de beurt!"
         },
         "data": {
         "Key-1": "MVMobile",
         "Key-2": "MVMobile"
         },
         "to": "/topics/JavaSampleApproach",
         "priority": "high"
         }
         */

        Map<String, String> data = remoteMessage.getData();

        if (user != null) {
            sendNotification(remoteMessage, data);
        }
    }

    private void sendNotification(RemoteMessage remoteMessage, Map<String, String> data) {
        Log.d(TAG, "Sending notification - title: " + data.get("title") + "    body: " + data.get("body") + "            ID of GAME: " + data.get("id"));
        Bundle bundle = new Bundle();

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtras(bundle);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(data.get("title"))
                .setContentText(data.get("body"))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                //.setContentInfo("Hello")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_custom))
                .setLights(Color.CYAN, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                //.setNumber(Integer.parseInt(data.get("id")))
                .setSmallIcon(R.drawable.ic_notification_white_24dp);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription(CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(R.color.colorAccent);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(Integer.parseInt(data.get("id")), notificationBuilder.build());
    }

}
