package be.kdg.mvmobile.injection;

import javax.inject.Singleton;

import be.kdg.mvmobile.activities.friends.FriendListActivity;
import be.kdg.mvmobile.activities.login.LoginActivity;
import be.kdg.mvmobile.activities.mainmenu.MainMenuActivity;
import be.kdg.mvmobile.activities.mystatistics.MyStatisticsActivity;
import be.kdg.mvmobile.activities.register.RegisterActivity;
import be.kdg.mvmobile.firebase.MyFirebaseMessagingService;
import dagger.Component;

@Singleton
@Component(modules = ServiceModule.class)
public interface AppComponent {
    void inject(LoginActivity activity);
    void inject(RegisterActivity activity);
    void inject(MainMenuActivity activity);
    void inject(MyStatisticsActivity activity);
    void inject(FriendListActivity activity);
    void inject(MyFirebaseMessagingService service);
}
