package be.kdg.mvmobile.injection;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import javax.inject.Singleton;

import be.kdg.mvmobile.services.FriendService;
import be.kdg.mvmobile.services.GameService;
import be.kdg.mvmobile.services.UserService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class ServiceModule {
    private String auth = "Basic jwtclientid:XY7kmzoNzl100";

    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
    // set your desired log level

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addNetworkInterceptor(new StethoInterceptor())
            /*
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    Request.Builder newRequest = request.newBuilder().header("Authorization", auth);
                    return chain.proceed(newRequest.build());
                }
            })
            */
            .addInterceptor(logging)
            .build();

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        Retrofit retrofit = new Retrofit.Builder()
                /*
                 * Left the .baseUrl like this rather than strings.xml because we don't have the Context here
                 * and in order to reach the local resources we need to do a workaround:
                 * https://stackoverflow.com/questions/4253328/getstring-outside-of-a-context-or-activity
                 * https://stackoverflow.com/questions/4391720/how-can-i-get-a-resource-content-from-a-static-context/4391811#4391811
                 */
                //.baseUrl("https://machiavelli-talkative-panda.cfapps.io/")
                .baseUrl("http://10.0.2.2:5000/")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit;
    }

    @Provides
    @Singleton
    UserService provideUserService(Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }

    @Provides
    @Singleton
    FriendService provideFriendService(Retrofit retrofit) {
        return retrofit.create(FriendService.class);
    }
    @Provides
    @Singleton
    GameService provideGameService(Retrofit retrofit) {
        return retrofit.create(GameService.class);
    }
}
