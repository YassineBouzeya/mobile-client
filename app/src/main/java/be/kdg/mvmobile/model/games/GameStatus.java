package be.kdg.mvmobile.model.games;

public enum GameStatus {
    LOBBY(0), ONGOING(1), ENDED(2);
    private int i;

    GameStatus(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}
