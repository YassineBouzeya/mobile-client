package be.kdg.mvmobile.model.users;

import java.time.LocalDateTime;

public class ChatMessage {
    private int messageId;
    private String message;
    private LocalDateTime timestamp;

    public ChatMessage() {
    }

    public ChatMessage(int messageId, String message, LocalDateTime timestamp) {
        this.messageId = messageId;
        this.message = message;
        this.timestamp = timestamp;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
