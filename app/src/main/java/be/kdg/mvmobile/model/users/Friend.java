package be.kdg.mvmobile.model.users;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import be.kdg.mvmobile.database.MVDatabase;

@Table(database = MVDatabase.class)
public class Friend extends BaseModel {
    @PrimaryKey
    private int friendId;
    @Column
    private String email;
    @Column
    private String username;

    private Statistics statistics;



    public Friend() {
    }

    public Friend(String email, String username) {
        this.email = email;
        this.username = username;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }
}
