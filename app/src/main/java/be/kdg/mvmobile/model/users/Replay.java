package be.kdg.mvmobile.model.users;

public class Replay {
    private int replayId;
    private int daysOfLifeTime;

    public Replay() {
    }

    public Replay(int replayId, int daysOfLifeTime) {
        this.replayId = replayId;
        this.daysOfLifeTime = daysOfLifeTime;
    }

    public int getReplayId() {
        return replayId;
    }

    public void setReplayId(int replayId) {
        this.replayId = replayId;
    }

    public int getDaysOfLifeTime() {
        return daysOfLifeTime;
    }

    public void setDaysOfLifeTime(int daysOfLifeTime) {
        this.daysOfLifeTime = daysOfLifeTime;
    }
}
