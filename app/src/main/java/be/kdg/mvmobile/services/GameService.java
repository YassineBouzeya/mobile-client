package be.kdg.mvmobile.services;

import java.util.List;

import be.kdg.mvmobile.services.dto.GameDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import ua.naiksoftware.stomp.StompClient;

public interface GameService {
    @GET("/gameOverview")
    Call<List<GameDto>> getFriends(@Header("Authorization") String authToken);
}
