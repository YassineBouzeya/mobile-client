package be.kdg.mvmobile.services;

import be.kdg.mvmobile.model.users.Token;
import be.kdg.mvmobile.services.dto.DeviceDto;
import be.kdg.mvmobile.services.dto.StatisticsDto;
import be.kdg.mvmobile.services.dto.UserDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserService {
    @POST("/oauth/token")
    @FormUrlEncoded
    @Headers({
            "Authorization: Basic and0Y2xpZW50aWQ6WFk3a216b056bDEwMA=="
    })
    Call<Token> login(@Field("username") String username,
                      @Field("password") String password,
                      @Field("grant_type") String grant_type);

    @GET("/getUser")
    Call<UserDto> getUser(@Header("Authorization") String authToken);

    @GET("/checkusername/{username}")
    Call<Boolean> checkUsername(@Path("username") String username);

    @GET("checkemail/{email}")
    Call<Boolean> checkEmail(@Path("email") String email);

    @POST("/register")
    Call<Void> register(@Body() UserDto userDto);

    @GET("/getStatistics/{username}")
    Call<StatisticsDto> getStatistics(@Header("Authorization") String authToken, @Path("username") String username);

    @POST("/sendDeviceId")
    Call<Void> sendDeviceId(@Header("Authorization") String authToken, @Body DeviceDto deviceDto);
}
