package be.kdg.mvmobile.services.dto;

import java.util.List;

public class AndroidDataDto {
    private String isChooseCardTurn;
    private GameDto gameDto;
    private List<BuildingCardDto> buildingCards;

    public AndroidDataDto() {
    }

    public String getIsChooseCardTurn() {
        return isChooseCardTurn;
    }

    public void setIsChooseCardTurn(String isChooseCardTurn) {
        this.isChooseCardTurn = isChooseCardTurn;
    }

    public GameDto getGameDto() {
        return gameDto;
    }

    public void setGameDto(GameDto gameDto) {
        this.gameDto = gameDto;
    }

    public List<BuildingCardDto> getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(List<BuildingCardDto> buildingCards) {
        this.buildingCards = buildingCards;
    }
}
