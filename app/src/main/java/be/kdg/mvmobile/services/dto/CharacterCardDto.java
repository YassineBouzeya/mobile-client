package be.kdg.mvmobile.services.dto;

public class CharacterCardDto {
    private int id;
    private boolean isFaceUp;
    private boolean isKilled;
    private boolean isRobbed;
    private boolean isPlacedMiddle;
    private CharacterDto character;

    public CharacterCardDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFaceUp() {
        return isFaceUp;
    }

    public void setFaceUp(boolean faceUp) {
        isFaceUp = faceUp;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    public boolean isRobbed() {
        return isRobbed;
    }

    public void setRobbed(boolean robbed) {
        isRobbed = robbed;
    }

    public CharacterDto getCharacter() {
        return character;
    }

    public void setCharacter(CharacterDto character) {
        this.character = character;
    }

    public boolean isPlacedMiddle() {
        return isPlacedMiddle;
    }

    public void setPlacedMiddle(boolean placedMiddle) {
        isPlacedMiddle = placedMiddle;
    }
}
