package be.kdg.mvmobile.services.dto;

import java.util.Arrays;

public class GameDto {
    private int id;
    private String gameName;
    private BuildingCardDto[] buildingCards;
    private CharacterCardDto[] characterCards;
    private PlayerDto[] players;
    private int turnDuration;
    private int reqBuildings;
    private int maxPlayers;
    private int nrOfPlayers;
    private int joinType;
    private int status;
    private int playerTurn;

    public GameDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }

    public int getReqBuildings() {
        return reqBuildings;
    }

    public void setReqBuildings(int reqBuildings) {
        this.reqBuildings = reqBuildings;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getJoinType() {
        return joinType;
    }

    public void setJoinType(int joinType) {
        this.joinType = joinType;
    }

    public int getNrOfPlayers() {
        return nrOfPlayers;
    }

    public void setNrOfPlayers(int nrOfPlayers) {
        this.nrOfPlayers = nrOfPlayers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BuildingCardDto[] getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(BuildingCardDto[] buildingCards) {
        this.buildingCards = buildingCards;
    }

    public CharacterCardDto[] getCharacterCards() {
        return characterCards;
    }

    public void setCharacterCards(CharacterCardDto[] characterCards) {
        this.characterCards = characterCards;
    }

    public PlayerDto[] getPlayers() {
        return players;
    }

    public void setPlayers(PlayerDto[] players) {
        this.players = players;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(int playerTurn) {
        this.playerTurn = playerTurn;
    }

    @Override
    public String toString() {
        return "GameDto{" +
                "id=" + id +
                ", gameName='" + gameName + '\'' +
                ", buildingCards=" + Arrays.toString(buildingCards) +
                ", characterCards=" + Arrays.toString(characterCards) +
                ", players=" + Arrays.toString(players) +
                ", turnDuration=" + turnDuration +
                ", reqBuildings=" + reqBuildings +
                ", maxPlayers=" + maxPlayers +
                ", nrOfPlayers=" + nrOfPlayers +
                ", joinType=" + joinType +
                ", status=" + status +
                ", playerTurn=" + playerTurn +
                '}';

    }
}
