package be.kdg.mvmobile.services.dto;

public class PlayerDto {
    private int id;
    private String userName;
    private String avatar;
    private UserDto[] users;
    private CharacterCardDto characterCard;
    private BuildingCardDto[] buildingCards;
    private boolean hasCrown;
    private int orderNr;
    private int nrOfCoins;
    private int endGamePoints;
    private boolean isHost;

    public PlayerDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDto[] getUsers() {
        return users;
    }

    public void setUsers(UserDto[] users) {
        this.users = users;
    }

    public CharacterCardDto getCharacterCard() {
        return characterCard;
    }

    public void setCharacterCard(CharacterCardDto characterCard) {
        this.characterCard = characterCard;
    }

    public BuildingCardDto[] getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(BuildingCardDto[] buildingCards) {
        this.buildingCards = buildingCards;
    }

    public boolean isHasCrown() {
        return hasCrown;
    }

    public void setHasCrown(boolean hasCrown) {
        this.hasCrown = hasCrown;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    public int getNrOfCoins() {
        return nrOfCoins;
    }

    public void setNrOfCoins(int nrOfCoins) {
        this.nrOfCoins = nrOfCoins;
    }

    public int getEndGamePoints() {
        return endGamePoints;
    }

    public void setEndGamePoints(int endGamePoints) {
        this.endGamePoints = endGamePoints;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setIsHost(boolean host) {
        isHost = host;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setHost(boolean host) {
        isHost = host;
    }
}
