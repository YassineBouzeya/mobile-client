package be.kdg.mvmobile.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import be.kdg.mvmobile.R;
import be.kdg.mvmobile.model.users.Token;
import be.kdg.mvmobile.model.users.User;

public class MySharedPreferences {
    private final Context context;
    private SharedPreferences sharedPreferences;

    public MySharedPreferences(Context context, SharedPreferences sharedPreferences) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
    }

    public Token readTokenSP() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(context.getString(R.string.saved_key_Token), "");
        Token token = gson.fromJson(json, Token.class);

        return token;
    }

    public void writeTokenSP(Token token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String jsonToken = gson.toJson(token);
        editor.putString(context.getString(R.string.saved_key_Token), jsonToken);

        editor.apply();
    }

    public User readUserSP() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(context.getString(R.string.saved_key_User), "");
        User user = gson.fromJson(json, User.class);

        return user;
    }

    public void writeUserSP(User user) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String jsonUser = gson.toJson(user);
        editor.putString(context.getString(R.string.saved_key_User), jsonUser);

        editor.apply();
    }

    public int readConnectedGameIdSP() {
        return sharedPreferences.getInt(context.getString(R.string.saved_key_connectedGameId), 0);
    }

    public void writeConnectedGameIdSP(int gameId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getString(R.string.saved_key_connectedGameId), gameId);
        editor.apply();
    }

    public void clearAllSP() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();

        editor.apply();
    }

}
